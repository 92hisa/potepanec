require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, name: "Bags") }
  let!(:product) { create(:product, taxons: [taxon], name: "Rails bag", price: 1.11) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "正常なレスポンスを返すこと" do
    expect(response).to have_http_status 200
  end

  it "共通テンプレートが正常に反映されること" do
    expect(response).to render_template :show
  end

  it "ページタイトルが正しく表示されること" do
    expect(response.body).to match(/<title>Rails bag | BIGBAG Store<\/title>/i)
  end

  it "商品情報が表示されていること" do
    expect(response.body).to include product.name
    expect(response.body).to include product.display_price.to_s
  end

  it "サイドバーの商品カテゴリーにtaxonomy名が表示されていること" do
    expect(response.body).to include taxonomy.name
  end

  it "サイドバーの商品カテゴリーにtaxon名が表示されていること" do
    expect(response.body).to include taxon.name
  end

  it "サイドバーの商品カテゴリーにtaxonに紐付いた商品個数が表示されていること" do
    expect(response.body).to include taxon.products.length.to_s
  end
end
