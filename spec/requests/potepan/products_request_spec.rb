require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], name: "sample", price: 1.23, description: "it is sample product") }
  let!(:related_product) { create(:product, taxons: [taxon], name: "sample related product", price: 9.99) }

  before do
    get potepan_product_path(product.id)
  end

  it "正常にレスポンスを返すこと" do
    expect(response).to be_successful
    expect(response).to have_http_status 200
  end

  it "共通テンプレートが正常に反映されること" do
    expect(response).to render_template :show
  end

  it "ページタイトルが正しく表示されること" do
    expect(response.body).to match(/<title>sample | BIGBAG Store<\/title>/i)
  end

  it "商品詳細が表示されていること" do
    expect(response.body).to include "sample"
    expect(response.body).to include "1.23"
    expect(response.body).to include "it is sample product"
  end

  it "関連商品の商品名と価格が表示されていること" do
    expect(response.body).to include "sample related product"
    expect(response.body).to include "9.99"
  end
end
