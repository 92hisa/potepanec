require 'rails_helper'

RSpec.describe "Potepan::ProductDecorator", type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }
  let(:unrelated_product) { create(:product) }

  it "商品(product)と同じカテゴリーに属する関連商品(related_product)が表示されること" do
    expect(product.related_products).to include related_product
  end

  it "商品(product)と異なるカテゴリーに属する関連商品(related_product)が表示されないこと" do
    expect(product.related_products).not_to include unrelated_product
  end

  it "商品(product)が関連商品(related_product)に含まれないこと" do
    expect(product.related_products).not_to include product
  end
end
