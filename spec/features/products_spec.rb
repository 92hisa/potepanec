require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], name: "sample", price: 1.23, description: "it is sample product") }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_products) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  feature "商品情報の表示のテスト" do
    scenario "タイトル名が正しく表示されること" do
      expect(page).to have_title "sample | BIGBAG Store"
    end

    scenario "商品名と価格が表示されること" do
      expect(page).to have_content product.name
      expect(page).to have_content product.price.to_s
    end

    scenario "関連商品が正しく表示されていること" do
      within ".productsContent" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price.to_s
      end
    end
  end

  feature "画面遷移のテスト" do
    scenario "navbarのHomeをクリックするとトップページへ遷移すること" do
      within(".navbar-right") do
        click_on 'Home'
        expect(current_path).to eq potepan_index_path
      end
    end

    scenario "lightSectionのHomeをクリックするとトップページへ遷移すること" do
      within(".breadcrumb") do
        click_on 'Home'
        expect(current_path).to eq potepan_index_path
      end
    end

    scenario "「一覧ページへ戻る」をクリックすると商品一覧ページへ遷移すること" do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario "関連商品名をクリックすると商品詳細ページへ遷移すること" do
      within ".productsContent" do
        click_on related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end

    scenario "関連商品の価格をクリックすると商品詳細ページへ遷移すること" do
      within ".productsContent" do
        click_on related_product.display_price
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end

    scenario "関連商品画像をクリックすると商品詳細ページへ遷移すること" do
      within ".productsContent" do
        click_on "image of #{related_product.name}"
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end

  feature "関連商品の表示数のテスト" do
    let!(:related_product) { create_list(:product, 5, taxons: [taxon]) }

    scenario "関連商品が最大４つまで表示されること" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  scenario "関連商品ではない商品は表示されていないこと" do
    within ".productsContent" do
      expect(page).not_to have_content unrelated_products
    end
  end
end
