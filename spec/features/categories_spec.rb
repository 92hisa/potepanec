require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, name: "Bags") }
  let!(:product) { create(:product, taxons: [taxon], name: "Rails bag", price: 1.11) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "navbarのHomeをクリックするとトップページへ遷移すること" do
    within(".navbar-right") do
      click_on 'Home'
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario "lightSectionのHomeをクリックするとトップページへ遷移すること" do
    within(".breadcrumb") do
      click_on 'Home'
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario "タイトル名が正しく表示されること" do
    expect(page).to have_title "Bags | BIGBAG Store"
  end

  scenario "商品名と価格は商品詳細ページのリンクであること" do
    expect(page).to have_link product.name, href: potepan_product_path(product.id)
    expect(page).to have_link product.display_price, href: potepan_product_path(product.id)
  end

  scenario "サイドバーにtaxonomy名、taxon名が表示されること" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
  end

  scenario "サイドバーのtaxon名をクリックすると商品一覧ページへ遷移すること" do
    click_link taxon.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario "サイドバーのtaxon名をクリックすると商品名と価格が表示されること" do
    click_link taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.price.to_s
  end
end
