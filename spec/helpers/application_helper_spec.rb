require 'rails_helper'

RSpec.describe "ApplicationHelper", type: :helper do
  include ApplicationHelper
  describe "full_title_method" do
    context "ページタイトルに商品名が含まれる場合" do
      it "ページタイトルに商品名とBIGBAGが含まれること" do
        expect(full_title("商品名")).to eq "商品名 | BIGBAG Store"
      end
    end

    context "ページタイトルに商品名が含まれない場合" do
      it "ページタイトルにBIGBAGが含まれること" do
        expect(full_title('')).to eq "BIGBAG Store"
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
      it "ページタイトルに商品名が含まれないこと" do
        expect(full_title('')).not_to include("商品名")
      end
    end
  end
end
